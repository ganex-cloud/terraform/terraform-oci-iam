module "iam_group-dev" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/group?ref=master"
  compartment_id = var.tenancy_ocid
  description    = "dev"
  name           = "dev"
  user_ids       = [module.iam_user-user1.user_id]
}
