variable "group_create" {
  description = "Create the user or not. If true, the user must have permissions to create the user; If false, user data will be returned about the user if it exists, if not found, then an empty string will be returned for the user ID."
  default     = true
}

variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the parent compartment containing the compartment."
  type        = string
  default     = ""
}

variable "name" {
  description = "(Required) (Updatable) The name you assign to the compartment during creation. The name must be unique across all compartments in the parent compartment. Avoid entering confidential information."
  type        = string
  default     = ""
}

variable "description" {
  description = "(Required) (Updatable) The description you assign to the compartment during creation. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "user_ids" {
  description = "List of user' ocids. "
  type        = list(string)
  default     = []
}
