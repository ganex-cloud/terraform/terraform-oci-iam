resource "oci_identity_group" "this" {
  count          = var.group_create ? 1 : 0
  compartment_id = var.compartment_id
  description    = var.description
  name           = var.name
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
}

resource "oci_identity_user_group_membership" "this" {
  user_id  = var.user_ids[0]
  group_id = oci_identity_group.this[0].id
}
