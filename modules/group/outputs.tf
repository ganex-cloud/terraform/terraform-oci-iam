output "group_id" {
  value = oci_identity_group.this[0].id
}

output "group_name" {
  value = oci_identity_group.this[0].name
}
