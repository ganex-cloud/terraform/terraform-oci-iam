module "iam_user-user1" {
  source                   = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/user?ref=master"
  compartment_id           = var.tenancy_ocid
  description              = "user1"
  name                     = "user1"
  can_use_smtp_credentials = false
}
