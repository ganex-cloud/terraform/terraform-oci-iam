output "user_id" {
  value = oci_identity_user.this[0].id
}

output "user_name" {
  value = oci_identity_user.this[0].name
}
