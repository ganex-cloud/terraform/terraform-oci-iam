resource "oci_identity_user" "this" {
  count          = var.user_create ? 1 : 0
  compartment_id = var.compartment_id
  description    = var.description
  name           = var.name
  defined_tags   = var.defined_tags
  email          = var.email
  freeform_tags  = var.freeform_tags
}

resource "oci_identity_user_capabilities_management" "this" {
  user_id                      = oci_identity_user.this[0].id
  can_use_api_keys             = var.can_use_api_keys
  can_use_auth_tokens          = var.can_use_auth_tokens
  can_use_console_password     = var.can_use_console_password
  can_use_customer_secret_keys = var.can_use_customer_secret_keys
  can_use_smtp_credentials     = var.can_use_smtp_credentials
}
