variable "user_create" {
  description = "Create the user or not. If true, the user must have permissions to create the user; If false, user data will be returned about the user if it exists, if not found, then an empty string will be returned for the user ID."
  default     = true
}

variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the parent compartment containing the compartment."
  type        = string
  default     = ""
}

variable "name" {
  description = "(Required) (Updatable) The name you assign to the compartment during creation. The name must be unique across all compartments in the parent compartment. Avoid entering confidential information."
  type        = string
  default     = ""
}

variable "description" {
  description = "(Required) (Updatable) The description you assign to the compartment during creation. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "email" {
  description = "(Optional) (Updatable) The email you assign to the user. Has to be unique across the tenancy."
  type        = string
  default     = ""
}

variable "can_use_api_keys" {
  description = "(Optional) (Updatable) Indicates if the user can use API keys."
  type        = bool
  default     = true
}

variable "can_use_auth_tokens" {
  description = "(Optional) (Updatable) Indicates if the user can use SWIFT passwords / auth tokens."
  type        = bool
  default     = true
}

variable "can_use_console_password" {
  description = "(Optional) (Updatable) Indicates if the user can log in to the console."
  type        = bool
  default     = true
}

variable "can_use_customer_secret_keys" {
  description = "(Optional) (Updatable) Indicates if the user can use SigV4 symmetric keys."
  type        = bool
  default     = true
}

variable "can_use_smtp_credentials" {
  description = "(Optional) (Updatable) Indicates if the user can use SMTP passwords."
  type        = bool
  default     = true
}
