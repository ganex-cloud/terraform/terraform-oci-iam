module "iam_dynamic_group-dev" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/dynamic-group?ref=master"
  compartment_id = var.tenancy_ocid
  description    = "dev"
  name           = "dev"
  matching_rule  = "instance.compartment.id = ${module.iam_compartment.compartment_id}"
}
