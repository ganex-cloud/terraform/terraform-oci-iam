output "dynamic_group_id" {
  value = oci_identity_dynamic_group.this[0].id
}

output "dynamic_group_name" {
  value = oci_identity_dynamic_group.this[0].name
}
