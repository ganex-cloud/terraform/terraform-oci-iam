resource "oci_identity_dynamic_group" "this" {
  count          = var.dynamic_group_create ? 1 : 0
  compartment_id = var.compartment_id
  description    = var.description
  matching_rule  = var.matching_rule
  name           = var.name
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
}
