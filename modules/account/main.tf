resource "oci_identity_authentication_policy" "this" {
  compartment_id = var.compartment_id
  password_policy {
    is_lowercase_characters_required = var.is_lowercase_characters_required
    is_numeric_characters_required   = var.is_numeric_characters_required
    is_special_characters_required   = var.is_special_characters_required
    is_uppercase_characters_required = var.is_uppercase_characters_required
    is_username_containment_allowed  = var.is_username_containment_allowed
    minimum_password_length          = var.minimum_password_length
  }
}
