module "iam_account" {
  source                           = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/account?ref=master"
  compartment_id                   = var.tenancy_ocid
  is_lowercase_characters_required = true
  is_numeric_characters_required   = true
  is_special_characters_required   = true
  is_uppercase_characters_required = true
  is_username_containment_allowed  = false
  minimum_password_length          = 12
}
