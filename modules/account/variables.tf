variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the parent compartment containing the compartment."
  type        = string
  default     = ""
}

variable "is_lowercase_characters_required" {
  description = "(Optional) (Updatable) At least one lower case character required."
  type        = bool
  default     = true
}

variable "is_numeric_characters_required" {
  description = "(Optional) (Updatable) At least one numeric character required."
  type        = bool
  default     = true
}

variable "is_special_characters_required" {
  description = "(Optional) (Updatable) At least one special character required."
  type        = bool
  default     = true
}

variable "is_uppercase_characters_required" {
  description = "(Optional) (Updatable) At least one uppercase character required."
  type        = bool
  default     = true
}

variable "is_username_containment_allowed" {
  description = "(Optional) (Updatable) User name is allowed to be part of the password."
  type        = bool
  default     = false
}

variable "minimum_password_length" {
  description = "(Optional) (Updatable) Minimum password length required."
  type        = number
  default     = 12
}
