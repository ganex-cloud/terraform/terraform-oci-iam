resource "oci_identity_compartment" "this" {
  compartment_id = var.compartment_id
  name           = var.name
  description    = var.description
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags
  enable_delete  = var.enable_delete
}
