module "compartiment_infra" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/compartment?ref=master"
  compartment_id = "ROOT_COMPARTMENT"
  name           = "infra"
  description    = "Test"
  enable_delete  = true
}
