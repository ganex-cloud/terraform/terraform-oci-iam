output "compartment_id" {
  value = oci_identity_compartment.this.id
}

output "compartment_name" {
  value = oci_identity_compartment.this.name
}
