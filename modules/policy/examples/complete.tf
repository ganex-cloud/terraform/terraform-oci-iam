module "iam_policy_dev" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/policy?ref=master"
  compartment_id = module.iam_compartment.compartment_id
  description    = "dev"
  name           = "dev"
  statements     = ["Allow group ${module.iam_group-dev.group_name} to read instances in compartment ${module.iam_compartment.compartment_name}"]
}
