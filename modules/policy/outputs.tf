output "policy_id" {
  value = oci_identity_policy.this.id
}

output "policy_name" {
  value = oci_identity_policy.this.name
}
