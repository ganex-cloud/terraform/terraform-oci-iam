variable "compartment_id" {
  description = "(Required) The OCID of the compartment containing the policy (either the tenancy or another compartment)."
  type        = string
  default     = ""
}

variable "name" {
  description = "(Required) The name you assign to the policy during creation. The name must be unique across all policies in the tenancy and cannot be changed."
  type        = string
  default     = ""
}

variable "description" {
  description = "(Required) (Updatable) The description you assign to the compartment during creation. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "statements" {
  description = "(Required) (Updatable) An array of policy statements written in the policy language."
  type        = list(string)
  default     = []
}

variable "version_date" {
  description = "The version of the policy. "
  type        = string
  default     = null
}
