variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the parent compartment containing the compartment."
  type        = string
  default     = ""
}

variable "name" {
  description = "(Required) (Updatable) The name you assign to the compartment during creation. The name must be unique across all compartments in the parent compartment. Avoid entering confidential information."
  type        = string
  default     = ""
}

variable "description" {
  description = "(Required) (Updatable) The description you assign to the compartment during creation. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "public_source_list" {
  description = "(Optional) (Updatable) A list of allowed public IP addresses and CIDR ranges."
  type        = list(string)
  default     = []
}

variable "services" {
  description = "(Optional) (Updatable) A list of services allowed to make on-behalf-of requests. These requests can have different source IP addresses than those listed in the network source. "
  type        = list(string)
  default     = ["all"]
}

variable "virtual_source_list" {
  description = "(Optional) (Updatable) A list of allowed VCN OCID and IP range pairs."
  type        = any
  default     = []
}
