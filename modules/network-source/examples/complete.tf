module "iam_network_source-COMPANY" {
  source             = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/network-source?ref=master"
  compartment_id     = var.tenancy_ocid
  description        = "COMPANY"
  name               = "COMPANY"
  public_source_list = ["x.x.x.x/32"]
  virtual_source_list = [
    {
      ip_ranges = ["129.213.39.0/24"]
      vcn_id    = "ocid1.vcn.oc1.iad.aaaaaaaaexampleuniqueID"
    }
  ]
}
