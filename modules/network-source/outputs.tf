output "networksource_id" {
  value = oci_identity_network_source.this.id
}

output "networksource_name" {
  value = oci_identity_network_source.this.name
}

output "networksource_statements" {
  value = oci_identity_network_source.this.*.public_source_list
}
